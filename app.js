const express = require ("express");
const app = express();
const os = require("os");
const fs = require("fs");
const {exec} = require('child_process');
const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')

app.use(express.json());

let streamStart;
let serialPort;
const streamStop = 'pkill gst-launch-1.0'
const botStart = './server_commands/StartBot.sh'
const botStop = './server_commands/StopBot.sh'

if (os.hostname == 'g7'){
    console.log(`Running on Brett's Laptop`);
    //SerialPort = require('virtual-serialport');
    streamStart = './server_commands/laptop_stream.sh'
    serialPort = '/dev/ttyUSB0'
}else{
    console.log('Running on jetson.local');
    //SerialPort = require('serialport');
    streamStart = './server_commands/stream.sh'
    serialPort = '/dev/ttyS1'

}

const serialConnection = new SerialPort(serialPort, {baudRate: 115200})
const parser = new Readline()
serialConnection.pipe(parser)

parser.on('data', line => console.log(`> ${line}`))


function bashExec(script_location) {
    exec(script_location, (err, stdout, stderr) => {
        if (err) {
            console.log(`error: ${err.message}`);
        } else {
            console.log(`stdout: ${stdout.message}`);
            console.log(`stderr: ${stderr}`);
        }
    });
}


app.get('/stream/start', (req, res)=> {
    bashExec(streamStart)
    res.send('start signal sent');
})

app.get('/stream/stop', (req, res)=>{
    bashExec(stream_stop);
    res.send('stop signal sent');
})

app.get('/bot/start', (req, res)=>{
    bashExec(botStart);
    res.send('start signal sent');
});

app.get('/bot/stop', (reg, res)=>{
    bashExec(botStop);
    res.send('stop signal sent');
})

app.get('/serial/:command_string', (req, res) => {
    command_string = req.params.command_string;
    if ( (command_string[0] == 'L' || command_string[0] == 'R') && command_string[command_string.length -1] == '!'){
        serialConnection.write(command_string, (err) =>{
                if (err){
                    console.log(err.message);
                }
            })
        res.send(command_string);
    }else{
        res.send(`invalid: ${command_string}`);
    }
})

const port = process.env.PORT || 8001;
app.listen(port, ()=>{console.log(`Listening on ${port}...`)})
