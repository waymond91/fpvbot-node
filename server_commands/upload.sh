#!/bin/bash
if pgrep -x "openocd" > /dev/null
then
	echo "OpenOCD is already running"
else
	echo "Starting Openocd..."
	./debug_init.sh &
fi

arm-none-eabi-gdb /home/brett/jetson_bot/motion_controller/.pio/build/nucleo_f411re/firmware.elf --command=upload_commands.gdb

