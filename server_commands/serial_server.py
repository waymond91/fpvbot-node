#! /usr/bin/python

import serial
import string
import sys
import time
import socket

host = socket.gethostname()

if host == 'g7':
    import os, pty

    python_executable = '/home/brett/anaconda3/envs/fpvbot_server/bin/python'
    port_1 = 8002
    port_2 = 8003
    ip = '127.0.0.1'

    master, slave = pty.openpty()
    s_name = os.ttyname(slave)
    ser = serial.Serial(s_name)

else:
    port_1 = 10002
    port_2 = 10003
    ip = 'jetson.local'

    ser = serial.Serial(
        port='/dev/ttyTHS1',
        baudrate=115200,
        bytesize=serial.EIGHTBITS,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        timeout=1,
        xonxoff=False,
        rtscts=False,
        dsrdtr=False,
        writeTimeout=2
    )

def main():
    # Create a TCP/IP socket
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (ip, port_1)
        print >> sys.stderr, 'starting up on %s port %s' % server_address
        sock.bind(server_address)

    except socket.error:
        print >> sys.stderr, 'could not start on %s port %s' % server_address
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (ip, port_2)
        print >> sys.stderr, 'starting up on %s port %s' % server_address
        sock.bind(server_address)

    while True:
        sock.listen(1)
        # Wait for a connection
        print >>sys.stderr, 'waiting for a connection'
        connection, client_address = sock.accept()

        try:
            print >>sys.stderr, 'connection from', client_address
            message = ""
            while True:
                data = str(connection.recv(1))
                if data == "":
                    break
                if data != "\n":
                    message += data
                else:
                    print(message)
                    #connection.sendall(message)
                    ser.write(message)
                    while(True):
                        line  = ser.readline()
                        if line:
                            print(line)
                            connection.send(line)
                        else:
                            break
                    message = ""
        finally:
            print(message)
            connection.close()


if __name__ == '__main__':
    main()
