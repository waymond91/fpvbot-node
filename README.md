# FPVBot - NodeJS
This is a port of the robots higher-level networking, thread-management, and administrative functions from python to NodeJS.  
Node is really much better at thread management, asynchronous task management, and handles network interfaces like a champion.